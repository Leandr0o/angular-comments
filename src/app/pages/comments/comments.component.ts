import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/shared/services/user.service';
import { Comment } from './shared/models/comment.interface';
import { User } from './shared/models/user.interface';
import { CommentService } from './shared/services/comment.service';

@Component({
  selector: 'comments-page',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
})
export class CommentsComponent implements OnInit, OnDestroy {
  newComment = '';
  focusId = '';

  comments: Comment[] | null = null;
  user: User | null = null;

  userSubscription: Subscription | null = null;

  constructor(
    private commentService: CommentService,
    private userService: UserService
  ) {}

  async ngOnInit(): Promise<void> {
    this.userSubscription = this.userService.currentUser.subscribe((value) => {
      if (value) this.user = value;
    });
    this.comments = await this.commentService.getComments();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  async send(): Promise<void> {
    if (this.user) {
      await this.commentService.sendComment(this.newComment, this.user.id);
      this.comments = await this.commentService.getComments();
      this.newComment = '';
    }
  }
}
