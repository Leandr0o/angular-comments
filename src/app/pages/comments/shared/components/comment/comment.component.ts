import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/shared/services/user.service';
import { Comment } from '../../models/comment.interface';
import { User } from '../../models/user.interface';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'comment-component',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
})
export class CommentComponent implements OnInit, OnDestroy {
  @Input() comments: Comment[] | null = null;

  userSubscription: Subscription | null = null;
  focusIdSubscription: Subscription | null = null;

  user: User | null = null;
  focusId: string = '';
  responseBody: string = '';

  constructor(
    private commentService: CommentService,
    private userService: UserService
  ) {}

  async ngOnInit(): Promise<void> {
    this.userSubscription = this.userService.currentUser.subscribe((value) => {
      if (value) this.user = value;
    });
    this.focusIdSubscription = this.commentService.focusId.subscribe(
      (value) => {
        if (value) this.focusId = value;
      }
    );
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
    this.focusIdSubscription?.unsubscribe();
  }

  setFocus(id: string) {
    this.commentService.focusId.next(id);
  }

  async send(): Promise<void> {
    if (this.user) {
      this.comments = await this.commentService.sendComment(
        this.responseBody,
        this.user.id,
        this.focusId
      );
      this.responseBody = '';
    }
  }
}
