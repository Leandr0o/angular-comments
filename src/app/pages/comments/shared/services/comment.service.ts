import { BehaviorSubject } from 'rxjs';
import { sendFetch } from 'src/app/services/fetch.service';
import { parseUser } from 'src/app/shared/services/user.service';
import { Comment } from '../models/comment.interface';

export class CommentService {
  focusId = new BehaviorSubject<string | null>('');

  constructor() {}

  async getComments(): Promise<Comment[] | null> {
    try {
      return this.parseComments(await sendFetch('comments', 'GET'));
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  async sendComment(
    body: string,
    userId: string,
    previusId: string | null = null
  ): Promise<Comment[] | null> {
    try {
      return this.parseComments(
        await sendFetch('comments', 'POST', { body, previusId, userId })
      );
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  parseComments(comments: any[]): Comment[] {
    return comments.map((comment) => this.parseComment(comment));
  }

  parseComment(comment: any): Comment {
    const subComments = comment.sub_comments
      ? this.parseComments(comment.sub_comments)
      : undefined;
    const author = comment.author ? parseUser(comment.author) : undefined;
    return {
      id: comment.id,
      body: comment.body,
      author,
      subComments,
    };
  }
}
