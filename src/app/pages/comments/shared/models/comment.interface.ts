import { User } from './user.interface';

export interface Comment {
  id: string;
  body: string;
  author?: User;
  subComments?: Comment[];
}
