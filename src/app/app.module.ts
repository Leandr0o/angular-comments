import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { LoginComponent } from './shared/components/login/login.component';
import { UserService } from './shared/services/user.service';
import { CommentComponent } from './pages/comments/shared/components/comment/comment.component';
import { CommentService } from './pages/comments/shared/services/comment.service';

@NgModule({
  declarations: [
    AppComponent,
    CommentsComponent,
    LoginComponent,
    CommentComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [UserService, CommentService],
  bootstrap: [AppComponent],
})
export class AppModule {}
