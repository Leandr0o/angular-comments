import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/pages/comments/shared/models/user.interface';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  username: string = '';
  password: string = '';

  user: User | null = null;
  userSubscription: Subscription | null = null;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((value) => {
      if (value) this.user = value;
    });
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  async send(): Promise<void> {
    await this.userService.login(this.username, this.password);
  }
}
