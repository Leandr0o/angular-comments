import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/pages/comments/shared/models/user.interface';
import { sendFetch } from 'src/app/services/fetch.service';

export class UserService {
  currentUser = new BehaviorSubject<User | null>(null);

  constructor() {}

  async login(user: string, password: string): Promise<User | null> {
    try {
      const fullUser = parseUser(
        await sendFetch('login', 'POST', { user, password })
      );
      this.currentUser.next(fullUser);
      return fullUser;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}

export const parseUser = (user: any): User => {
  return { id: user.id, user: user.user, email: user.email };
};
